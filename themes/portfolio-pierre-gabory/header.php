<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
		<?php wp_head(); ?>
		<link href="https://fonts.googleapis.com/css?family=Space+Mono:400,700|Work+Sans:400,700" rel="stylesheet">
	</head>
	<body <?php body_class(); ?>>
		<div id="wrapper" class="hfeed">
			<header id="header" role="banner">
				<section id="branding">
					<div id="site-title">
						<?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
							<?php echo esc_html( get_bloginfo( 'name' ) ); ?>
						</a>
						<?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?>
					</div>
					<div id="site-description">
						<?php bloginfo( 'description' ); ?>
					</div>
				</section>
				<section id="introduction">
					<?php 
						echo __(get_page_by_title("introduction")->post_content);
					?>
				</section>
			</header>
			<nav id="menu">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>#container">Portfolio</a>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>#cv">CV</a>
				<?php get_search_form(); ?>
			</nav>
			<div id="container">