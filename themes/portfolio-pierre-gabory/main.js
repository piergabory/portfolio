var headerElement = document.getElementById('header');
var menuElement = document.getElementById('menu');
if (!document.body.classList.contains('single')){
 	var cvElement = document.getElementById('cv');
	var cvImg = cvElement.querySelector('img');
	var articleList = document.getElementsByTagName("article");
}

function loop() {
	if(window.scrollY > headerElement.clientHeight)
		menuElement.classList.add("sticky");
	else
		menuElement.classList.remove("sticky");

	if (!document.body.classList.contains('single')){
		var progress = ((window.scrollY + window.innerHeight - cvElement.offsetTop)/cvElement.clientHeight)
		if(progress > 0){
			cvImg.style.cssText = "transform: translateY("+ ((1-progress) * 100 + 20) +"%) rotateY("+ (progress * 100 -120) +"deg) rotateX("+ ((1-progress)*15) +"deg);"
		}
		for ( var i = 0; i < articleList.length; i++) 
		if(window.scrollY + window.innerHeight > articleList[i].offsetTop + articleList[i].clientHeight/2 ){
			articleList[i].style.transform= "scale(1)";
			articleList[i].style.opacity= 1;
		}
		else {
			articleList[i].style.transform= "scale(.8)";
			articleList[i].style.opacity= 0;
		}
	}
		
	window.requestAnimationFrame(loop);
}

window.requestAnimationFrame(loop);
