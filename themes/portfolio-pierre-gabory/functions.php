<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css',
    	array( $parent_style ),wp_get_theme()->get('Version'));
}

function wpdocs_mercury_scripts() {
    wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/main.js', array(), '1.0.0', true );
}

function modify_read_more_link() {
    return '<a class="more-link" href="' . get_permalink() . '">More</a>';
}

function filter_ptags_on_images($content){
    return preg_replace('/<p\b[^>]*>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '\1', $content);
}


add_action( 'wp_enqueue_scripts', 'wpdocs_mercury_scripts' );
add_filter( 'the_content_more_link', 'modify_read_more_link' );
add_filter( 'the_content', 'filter_ptags_on_images');
?>