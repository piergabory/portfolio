If you like Writer, but wish for a little more customization control, we've written WriterPro:

http://writerwp.co/

Features:

- Add Custom Logo
- Add Custom Background
- Add Custom Header Fonts (including Google Fonts)
- Add Custom Header and Link Colors
- Footer Credit Link Removed by Default

Request support and new features:

contact@writerwp.co

Thank you.