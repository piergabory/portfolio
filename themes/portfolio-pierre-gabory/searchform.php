<form role="search" id="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <input type="search" class="field"
        placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
        value="<?php echo get_search_query() ?>" name="s"
        title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    <input type="submit" class="submit"
        value="<?php echo esc_attr_x( 'Submit', 'submit button' ) ?>" />
</form>