</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
&copy; <?php echo date( 'Y' ); ?> <?php bloginfo( 'name' ); ?>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>